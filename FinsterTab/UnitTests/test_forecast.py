from unittest import TestCase
import pytest
import sqlalchemy
import pyodbc
from fetch import Fetch
from forecast import Forecast


class TestForecast(TestCase):

    conn_str = 'mssql+pyodbc://sa:Gmfintech18@localhost/GMFSP_db?driver=SQL+Server+Native+Client+11.0'

    def test_check_db_is_true(self):
        # Given
        fetch_type2 = 'forecast'
        f2 = Fetch(fetch_type2)
        forecast = Forecast(f2)

        # When
        db_checked = forecast.check_db()

        # Then
        self.assertFalse(db_checked, "should be true")

    def test_check_db_has_spy_ts_table(self):
        # Given
        fetch_type2 = 'forecast'
        f2 = Fetch(fetch_type2)
        forecast = Forecast(f2)
        # When
        spy_ts = "SPY" + '_TS'
        db_checked = forecast.check_db()
        has_spy_ts = forecast.engine.dialect.has_table(forecast.engine, spy_ts)

        # Then
        self.assertTrue(has_spy_ts, "should be true")

    def test_check_db_has_xph_ts_table(self):
        # Given
        fetch_type2 = 'forecast'
        f2 = Fetch(fetch_type2)
        forecast = Forecast(f2)
        # When
        xph_ts = "XPH" + '_TS'
        db_checked = forecast.check_db()
        has_xph_ts = forecast.engine.dialect.has_table(forecast.engine, xph_ts)

        # Then
        self.assertTrue(has_xph_ts, "should be true")

    def test_check_db_has_pfe_ts_table(self):
        # Given
        fetch_type2 = 'forecast'
        f2 = Fetch(fetch_type2)
        forecast = Forecast(f2)
        # When
        pfe_ts = "PFE" + '_TS'
        db_checked = forecast.check_db()
        has_pfe_ts = forecast.engine.dialect.has_table(forecast.engine, pfe_ts)

        # Then
        self.assertTrue(has_pfe_ts, "should be true")

    def test_check_db_has_other_ts_table(self):
        # Given
        fetch_type2 = 'forecast'
        f2 = Fetch(fetch_type2)
        forecast = Forecast(f2)
        # When
        other_ts = "OTHER" + '_TS'
        db_checked = forecast.check_db()
        has_other_ts = forecast.engine.dialect.has_table(forecast.engine, other_ts)

        # Then
        self.assertFalse(has_other_ts, "should be true")

    def test_compute_forecast(self):
        # Given
        fetch_type2 = 'forecast'
        f2 = Fetch(fetch_type2)
        forecast = Forecast(f2)

        # When
        ts = forecast.compute_forecast()

        # Then
        self.assertIsNone(ts, "return none")

    def test_compute_forecast_has_data(self):
        # Given
        fetch_type2 = 'forecast'
        f2 = Fetch(fetch_type2)
        forecast = Forecast(f2)

        fc_data = forecast.fc_data
        count = len(fc_data[0])
        # When
        ts = forecast.compute_forecast()

        newf2 = Fetch(fetch_type2)
        newforecast = Forecast(newf2)
        new_fc_data = newforecast.fc_data
        newcount = len(new_fc_data[0])

        # Then
        self.assertEqual(count, newcount)
