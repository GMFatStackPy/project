from unittest import TestCase
import pandas_datareader.data as dr
import pandas as pd
from datetime import datetime
import sqlalchemy
from executeSP import ExecuteSP


class TestExecuteSP(TestCase):

    conn_str = 'mssql+pyodbc://sa:Gmfintech18@localhost/GMFSP_db?driver=SQL+Server+Native+Client+11.0'
    e = ExecuteSP()
    e.exec_sp()

    def get_data(self, table_name):
        engine = sqlalchemy.create_engine(self.conn_str)
        query = 'SELECT * FROM %s' % table_name
        s = pd.read_sql_query(query, engine)
        return s

    def test_execute_sp_spy_pred_tableColumns_exists(self):
        # Given
        table_name = "dbo.SPY_Pred"

        # When
        data = self.get_data(table_name)

        # Then
        columns = {'tradeDate', 'closePrice', 'BUYweekApproach', 'week_Long', 'SELLweekApproach', 'week_Short',
                   'shortCrossOver', 'longCrossOver', 'highFRLLineLong', 'medFRLLineLong', 'lowFRLLineLong',
                   'highFRLLineOneK', 'medFRLLineOneK', 'lowFRLLineOneK', 'momentumA', 'momentumB',
                   'rateOfChange', 'ActualChange'}
        self.assertTrue(columns.issubset(data.columns))

    def test_execute_sp_xph_pred_tableColumns_exists(self):
        # Given
        table_name = "dbo.XPH_Pred"

        # When
        data = self.get_data(table_name)

        # Then
        columns = {'tradeDate', 'closePrice', 'BUYweekApproach', 'week_Long', 'SELLweekApproach', 'week_Short',
                   'shortCrossOver', 'longCrossOver', 'highFRLLineLong', 'medFRLLineLong', 'lowFRLLineLong',
                   'highFRLLineOneK', 'medFRLLineOneK', 'lowFRLLineOneK', 'momentumA', 'momentumB', 'rateOfChange',
                   'ActualChange'}
        self.assertTrue(columns.issubset(data.columns))

    def test_execute_sp_pfe_pred_tableColumns_exists(self):
        # Given
        table_name = "dbo.PFE_Pred"

        # When
        data = self.get_data(table_name)

        # Then
        columns = {'tradeDate', 'closePrice', 'BUYweekApproach', 'week_Long', 'SELLweekApproach', 'week_Short',
                   'shortCrossOver', 'longCrossOver', 'highFRLLineLong', 'medFRLLineLong', 'lowFRLLineLong',
                   'highFRLLineOneK', 'medFRLLineOneK', 'lowFRLLineOneK', 'momentumA', 'momentumB', 'rateOfChange',
                   'ActualChange'}
        self.assertTrue(columns.issubset(data.columns))

    def test_execute_wrong_column_sp_pfe_pred_tableColumns_exists(self):
        # Given
        table_name = "dbo.PFE_Pred"

        # When
        data = self.get_data(table_name)
        # Wrong column name tradeDate -> wrongDate
        # Then
        columns = {'wrongDate', 'closePrice', 'BUYweekApproach', 'week_Long', 'SELLweekApproach', 'week_Short',
                   'shortCrossOver', 'longCrossOver', 'highFRLLineLong', 'medFRLLineLong', 'lowFRLLineLong',
                   'highFRLLineOneK', 'medFRLLineOneK', 'lowFRLLineOneK', 'momentumA', 'momentumB', 'rateOfChange',
                   'ActualChange'}
        self.assertFalse(columns.issubset(data.columns))

    def test_execute_wrong_column_sp_spy_pred_tableColumns_exists(self):
        # Given
        table_name = "dbo.SPY_Pred"

        # When
        data = self.get_data(table_name)
        # Wrong column name tradeDate -> wrongDate
        # Then
        columns = {'wrongDate', 'closePrice', 'BUYweekApproach', 'week_Long', 'SELLweekApproach', 'week_Short',
                   'shortCrossOver', 'longCrossOver', 'highFRLLineLong', 'medFRLLineLong', 'lowFRLLineLong',
                   'highFRLLineOneK', 'medFRLLineOneK', 'lowFRLLineOneK', 'momentumA', 'momentumB', 'rateOfChange',
                   'ActualChange'}
        self.assertFalse(columns.issubset(data.columns))

    def test_execute_wrong_column_sp_xph_pred_tableColumns_exists(self):
        # Given
        table_name = "dbo.XPH_Pred"

        # When
        data = self.get_data(table_name)
        # Wrong column name tradeDate -> wrongDate
        # Then
        columns = {'wrongDate', 'closePrice', 'BUYweekApproach', 'week_Long', 'SELLweekApproach', 'week_Short',
                   'shortCrossOver', 'longCrossOver', 'highFRLLineLong', 'medFRLLineLong', 'lowFRLLineLong',
                   'highFRLLineOneK', 'medFRLLineOneK', 'lowFRLLineOneK', 'momentumA', 'momentumB', 'rateOfChange',
                   'ActualChange'}
        self.assertFalse(columns.issubset(data.columns))

    def test_execute_sp_pfe_pred2_tableColumns_exists(self):
        # Given
        table_name = "dbo.PFE_Pred2"

        # When
        data = self.get_data(table_name)

        # Then
        columns = {'tradeDate', 'closePrice', 'momentumA', 'momentumB', 'sPeak',
                   'sTrough', 'fibExtHighNeg', 'fibExtLowNeg', 'fibExtHighPos',
                   'fibExtLowPos', 'hunOscillator', 'oneKOscillator'}
        self.assertTrue(columns.issubset(data.columns))

    def test_execute_sp_spy_pred2_tableColumns_exists(self):
        # Given
        table_name = "dbo.SPY_Pred2"

        # When
        data = self.get_data(table_name)

        # Then
        columns = {'tradeDate', 'closePrice', 'momentumA', 'momentumB', 'sPeak',
                   'sTrough', 'fibExtHighNeg', 'fibExtLowNeg', 'fibExtHighPos',
                   'fibExtLowPos', 'hunOscillator', 'oneKOscillator'}
        self.assertTrue(columns.issubset(data.columns))

    def test_execute_sp_xph_pred2_tableColumns_exists(self):
        # Given
        table_name = "dbo.XPH_Pred2"

        # When
        data = self.get_data(table_name)

        # Then
        columns = {'tradeDate', 'closePrice', 'momentumA', 'momentumB', 'sPeak',
                   'sTrough', 'fibExtHighNeg', 'fibExtLowNeg', 'fibExtHighPos',
                   'fibExtLowPos', 'hunOscillator', 'oneKOscillator'}
        self.assertTrue(columns.issubset(data.columns))

    def test_execute_wrong_columns_sp_pfe_pred2_tableColumns_exists(self):
        # Given
        table_name = "dbo.PFE_Pred2"

        # When
        data = self.get_data(table_name)

        # Then
        # Wrong column name tradeDate -> wrongDate
        columns = {'wrongDate', 'closePrice', 'momentumA', 'momentumB', 'sPeak',
                   'sTrough', 'fibExtHighNeg', 'fibExtLowNeg', 'fibExtHighPos',
                   'fibExtLowPos', 'hunOscillator', 'oneKOscillator'}
        self.assertFalse(columns.issubset(data.columns))

    def test_execute_wrong_columns_sp_spy_pred2_tableColumns_exists(self):
        # Given
        table_name = "dbo.SPY_Pred2"

        # When
        data = self.get_data(table_name)

        # Then
        # Wrong column name tradeDate -> wrongDate
        columns = {'wrongDate', 'closePrice', 'momentumA', 'momentumB', 'sPeak',
                   'sTrough', 'fibExtHighNeg', 'fibExtLowNeg', 'fibExtHighPos',
                   'fibExtLowPos', 'hunOscillator', 'oneKOscillator'}
        self.assertFalse(columns.issubset(data.columns))

    def test_execute_wrong_columns_sp_xph_pred2_tableColumns_exists(self):
        # Given
        table_name = "dbo.XPH_Pred2"

        # When
        data = self.get_data(table_name)

        # Then
        # Wrong column name tradeDate -> wrongDate
        columns = {'wrongDate', 'closePrice', 'momentumA', 'momentumB', 'sPeak',
                   'sTrough', 'fibExtHighNeg', 'fibExtLowNeg', 'fibExtHighPos',
                   'fibExtLowPos', 'hunOscillator', 'oneKOscillator'}
        self.assertFalse(columns.issubset(data.columns))