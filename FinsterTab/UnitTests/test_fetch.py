from unittest import TestCase
import pytest
import sqlalchemy
import pyodbc
from fetch import Fetch


class TestFetch(TestCase):

    conn_str = 'mssql+pyodbc://sa:Gmfintech18@localhost/GMFSP_db?driver=SQL+Server+Native+Client+11.0'

    def test_sql_engine_has_correct_url(self):
        # Given
        expected_engine = sqlalchemy.create_engine(self.conn_str)
        fetch_type2 = 'forecast'
        f2 = Fetch(fetch_type2)

        # When
        actual_engine = f2.sql_engine()

        # Then
        self.assertEqual(actual_engine.url, expected_engine.url, "same url")

    def test_get_data_sources_failure_when_using_wrong_table_name(self):
        # Given
        engine = sqlalchemy.create_engine(self.conn_str)

        # When
        fetch_type2 = 'forecast'
        f2 = Fetch(fetch_type2)
        table_name = "dbo.anything"
        sources = None
        with pytest.raises(sqlalchemy.exc.ProgrammingError):
            sources = f2.get_datasources(engine, table_name)

        # Then
        self.assertIsNone(sources, "sources not found")

    def test_get_data_sources_has_three_items(self):
        # Given
        engine = sqlalchemy.create_engine(self.conn_str)

        # When
        fetch_type2 = 'forecast'
        f2 = Fetch(fetch_type2)
        table_name = "dbo.FinDataSources"
        sources = f2.get_datasources(engine, table_name)

        # Then
        self.assertEqual(len(sources), 3, "should have 3 tables names")

    def test_get_data_sources_has_correct_ticker(self):
        # Given
        engine = sqlalchemy.create_engine(self.conn_str)

        # When
        fetch_type2 = 'forecast'
        f2 = Fetch(fetch_type2)
        table_name = "dbo.FinDataSources"
        sources = f2.get_datasources(engine, table_name)

        # Then
        tickerSPY = sources.iat[0, sources.columns.get_loc('dataSource')]
        tickerXPH = sources.iat[1, sources.columns.get_loc('dataSource')]
        tickerPFE = sources.iat[2, sources.columns.get_loc('dataSource')]
        self.assertEqual(tickerSPY, 'SPY', "SPY")
        self.assertEqual(tickerXPH, 'XPH', "XPH")
        self.assertEqual(tickerPFE, 'PFE', "PFE")

    def test_get_data_has_three_dataframe_len_when_fetch_type_is_forecast(self):
        # Given
        engine = sqlalchemy.create_engine(self.conn_str)
        fetch_type2 = 'forecast'
        f2 = Fetch(fetch_type2)
        table_name = "dbo.FinDataSources"
        sources = f2.get_datasources(engine, table_name)

        # When
        actual_data = f2.get_data(engine, sources)

        # Then
        self.assertEqual(len(actual_data), 3, "should have items")

    def test_get_data_has_three_dataframe_len_when_fetch_type_is_strategy(self):
        # Given
        engine = sqlalchemy.create_engine(self.conn_str)
        fetch_type2 = 'strategy'
        f2 = Fetch(fetch_type2)
        table_name = "dbo.FinDataSources"
        sources = f2.get_datasources(engine, table_name)

        # When
        actual_data = f2.get_data(engine, sources)

        # Then
        self.assertEqual(len(actual_data), 3, "should have items")

    def test_get_data_return_error_when_wrong_fetch_type(self):
        # Given
        engine = sqlalchemy.create_engine(self.conn_str)
        fetch_type2 = 'anything'
        f2 = Fetch(fetch_type2)
        table_name = "dbo.FinDataSources"
        sources = f2.get_datasources(engine, table_name)

        # When
        actual_data = f2.get_data(engine, sources)

        # Then
        self.assertEqual(actual_data, "Error: No fetch type defined for financial data", "should have items")