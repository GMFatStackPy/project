import sqlalchemy as sal


class TradingStrategies:
    def __init__(self, f1):
        self.engine = f1.sql_engine()
        self.sources = f1.get_datasources(self.engine, 'dbo.FinDataSources')
        self.fc_data = f1.get_data(self.engine, self.sources)

    def compute_ts(self):
        """
        Method to compute the Cross Moving Average (CMA) and Fibonacci Retracement Lines (FRL)
        Pandas utilizes SciPy.Signal functions to perform numerical computations on data (i.e. rolling, tail)

        :return: none
        """
        # Cross Moving Average Variables (CMA)
        week_n = 7
        short_n = 20
        long_n = 100
        # Fibonacci Retracement Variables (FRL)
        low_n = 14
        high_n = 100
        k_n = 1000

        # Make copy of the financial data 2D Pandas Dataframe
        f_data = self.fc_data
        # n is the counter for the sources ticker symbols
        n = 0

        while n < len(self.sources.index):
            # CMA
            f_data[n]['wCMA'] = self.fc_data[n]['closePrice'].rolling(week_n).mean()
            f_data[n]['sCMA'] = self.fc_data[n]['closePrice'].rolling(short_n).mean()
            f_data[n]['lCMA'] = self.fc_data[n]['closePrice'].rolling(long_n).mean()
            # FRL
            f_data[n]['sTrough'] = self.fc_data[n]['closePrice'].tail(low_n).min()
            f_data[n]['sPeak'] = self.fc_data[n]['closePrice'].tail(low_n).max()
            f_data[n]['lTrough'] = self.fc_data[n]['closePrice'].tail(high_n).min()
            f_data[n]['lPeak'] = self.fc_data[n]['closePrice'].tail(high_n).max()
            f_data[n]['kTrough'] = self.fc_data[n]['closePrice'].tail(k_n).min()
            f_data[n]['kPeak'] = self.fc_data[n]['closePrice'].tail(k_n).max()

            # Use ticker symbol in dataSources table at position 'n' to name the new table.
            # Named <ticker>_TS for Trading Strategies
            ticker = self.sources.iat[n, self.sources.columns.get_loc('dataSource')]
            ts_table = ticker + '_TS'
            # Drop unnecessary column data
            f_data[n].drop(['openPrice', 'low', 'high', 'closePrice', 'volume'], axis=1, inplace=True)
            # Send computed Trading Strategies to GMFSP_db (dbo.<Ticker>_TS Table)
            # Replace if it exists and use tradeDate as the index
            f_data[n].to_sql(ts_table, self.engine, if_exists='replace', index_label='tradeDate',
                             dtype={'tradeDate': sal.Date, 'wCMA': sal.FLOAT, 'sCMA': sal.FLOAT, 'lCMA': sal.FLOAT,
                                    'sTrough': sal.FLOAT, 'sPeak': sal.FLOAT, 'lTrough': sal.FLOAT, 'lPeak': sal.FLOAT,
                                    'kTrough': sal.FLOAT,'kPeak': sal.FLOAT, 'oMov': sal.FLOAT})

            # TEST: Verify order of data (tradeDate ASC)
            # f_data[n].to_csv('f_data.csv')

            # Advance source ticker counter
            n = n + 1
