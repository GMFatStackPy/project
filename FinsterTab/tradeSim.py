import pandas as pd
from sqlalchemy import *
from datetime import timedelta


class TradeSim:
    def __init__(self, f):
        self.engine = f.sql_engine()
        self.sources = f.get_datasources(self.engine, 'dbo.FinDataSources')

    # Trading Simulation
    def trading_simulator(self):
        """
        Trading Simulator
        Executes the trade based on the combined CMA and FRL Trading Strategy Data
        Starts with $10,000

        :return: ReturnsData.csv File
        """
        # Cycle through the data sources
        i = 0
        while i < len(self.sources.index):
            # Simulated Bank Balance
            # Assign data source to ticker
            # Add proper prefix/postfix to ticker for database access
            # All Variables reset on each iteration (i.e. each fin instrument)
            bank = 10000
            position = 0
            qty = 0
            # Date Variables: Last day of Trading Strategy activity
            # Source Ticker Table Variables
            ticker = self.sources.iat[i, self.sources.columns.get_loc('dataSource')]
            ticker_act = 'dbo.' + ticker + '_Act'
            ticker_trades = ticker + '_Trades'
            ticker_trades_dbo = 'dbo.' + ticker_trades

            # Date Variables: Last day of Trading Strategy activity
            # From <ticker>_Act Table
            q_now = "SELECT TOP 1 tradeDate FROM %s ORDER BY tradeDate DESC" % ticker_act
            now = pd.read_sql_query(q_now, self.engine)
            now = now.iloc[0]['tradeDate']

            if self.engine.dialect.has_table(self.engine, ticker_trades, schema='dbo'):
                # Date Variables: Last day of Trading Strategy activity
                # From <ticker>_Trades Table
                q_start_act = "SELECT TOP 1 tradeDate, Act, Qty, BuyingPower, PortfolioValue FROM %s ORDER BY tradeDate DESC" % ticker_trades_dbo
                data = pd.read_sql_query(q_start_act, self.engine)
                start_date = data.iloc[0]['tradeDate']
                act_data = data.iloc[0]['Act']
                qty = data.iloc[0]['Qty']
                position = qty
                bank = data.iloc[0]['BuyingPower']
                value = data.iloc[0]['PortfolioValue']

                if act_data == 'HOLD':
                    query = "DELETE FROM %s WHERE tradeDate = '%s'" % (ticker_trades_dbo, start_date)
                    self.engine.execute(query)
                    start_date = start_date - timedelta(days=1)

                # Get Trading Strategy Activity From starting from start_date
                q_act = "SELECT * FROM %s WHERE tradeDate > '%s'" % (ticker_act, start_date)
                actions = pd.read_sql_query(q_act, self.engine)
            else:
                # Start date: Jan 1st of 2018
                start_date = "20180101"
                # Create <ticker>_Trades Table
                metadata = MetaData()
                ticker = Table(ticker_trades, metadata,
                               Column('tradeDate', Date, nullable=False),
                               Column('Act', String(20), nullable=True),
                               Column('Price', FLOAT, nullable=True),
                               Column('Qty', FLOAT, nullable=True),
                               Column('PositionsHeld', FLOAT, nullable=True),
                               Column('BuyingPower', FLOAT, nullable=True),
                               Column('PortfolioValue', FLOAT, nullable=True)
                               )
                ticker.create(self.engine)

                # Get Trading Strategy Activity From starting from start_date
                q2 = "SELECT * FROM %s WHERE tradeDate > '%s'" % (ticker_act, start_date)
                actions = pd.read_sql_query(q2, self.engine)

            # Cycle through the activity information
            x = 0
            while x < len(actions.index):
                # Create local variables for Trade: Date, Price, and Action
                t_date = actions.iat[x, actions.columns.get_loc('tradeDate')]
                t_price = actions.iat[x, actions.columns.get_loc('closePrice')]
                cma_act = actions.iat[x, actions.columns.get_loc('CMATradeAction')]
                frl_act = actions.iat[x, actions.columns.get_loc('FRLTradeAction')]
                frl_k_act = actions.iat[x, actions.columns.get_loc('FRL_1K_TradeAction')]

                # If Buy Opportunity and Bank is not empty
                if cma_act == 1 and frl_act == 1 and frl_k_act == 1 and bank != 0:
                    t_action = 'Buy All In'
                    qty = bank / t_price
                    position = qty * t_price
                    value = qty * t_price
                    bank = 0
                    total_value = bank + value
                    query = "INSERT INTO %s([tradeDate],[Act], [Price], [Qty], [PositionsHeld], [BuyingPower]," \
                            " [PortfolioValue]) " \
                            "values(?, ?, ?, ?, ?, ?, ?)" % ticker_trades_dbo
                    self.engine.execute(query, t_date, t_action, t_price, qty, position, bank, total_value)
                elif cma_act != 0 and frl_act != 0 and frl_k_act == 1 and bank != 0:
                    t_action = 'Buy All In'
                    qty = bank / t_price
                    position = qty
                    value = qty * t_price
                    bank = 0
                    total_value = bank + value
                    query = "INSERT INTO %s([tradeDate],[Act], [Price], [Qty], [PositionsHeld], [BuyingPower]," \
                            " [PortfolioValue]) " \
                            "values(?, ?, ?, ?, ?, ?, ?)" % ticker_trades_dbo
                    self.engine.execute(query, t_date, t_action, t_price, qty, position, bank, total_value)
                elif cma_act == 1 and frl_act != 0 and frl_k_act != 0 and bank != 0:
                    t_action = 'Buy All In'
                    qty = bank / t_price
                    position = qty
                    value = qty * t_price
                    bank = 0
                    total_value = bank + value
                    query = "INSERT INTO %s([tradeDate],[Act], [Price], [Qty], [PositionsHeld], [BuyingPower]," \
                            " [PortfolioValue]) " \
                            "values(?, ?, ?, ?, ?, ?, ?)" % ticker_trades_dbo
                    self.engine.execute(query, t_date, t_action, t_price, qty, position, bank, total_value)
                elif cma_act != 0 and frl_act == 1 and frl_k_act != 0 and bank != 0:
                    t_action = 'Buy All In'
                    qty = bank / t_price
                    position = qty
                    value = qty * t_price
                    bank = 0
                    total_value = bank + value
                    query = "INSERT INTO %s([tradeDate],[Act], [Price], [Qty], [PositionsHeld], [BuyingPower]," \
                            " [PortfolioValue]) " \
                            "values(?, ?, ?, ?, ?, ?, ?)" % ticker_trades_dbo
                    self.engine.execute(query, t_date, t_action, t_price, qty, position, bank, total_value)

                # Else if a Sell Opportunity and Position is not empty
                elif cma_act == 0 and frl_act == 0 and frl_k_act == 0 and position != 0:
                    # Set dictionary value for Act to Sell
                    t_action = 'Sell'
                    value = qty * t_price
                    position = 0
                    qty = 0
                    bank = bank + value
                    total_value = bank
                    query = "INSERT INTO %s([tradeDate],[Act], [Price], [Qty], [PositionsHeld], [BuyingPower]," \
                            " [PortfolioValue]) " \
                            "values(?, ?, ?, ?, ?, ?, ?)" % ticker_trades_dbo
                    self.engine.execute(query, t_date, t_action, t_price, qty, position, bank, total_value)
                elif cma_act != 1 and frl_act != 1 and frl_k_act == 0 and position != 0:
                    # Set dictionary value for Act to Sell
                    t_action = 'Sell'
                    value = qty * t_price
                    position = 0
                    qty = 0
                    bank = bank + value
                    total_value = bank
                    query = "INSERT INTO %s([tradeDate],[Act], [Price], [Qty], [PositionsHeld], [BuyingPower]," \
                            " [PortfolioValue]) " \
                            "values(?, ?, ?, ?, ?, ?, ?)" % ticker_trades_dbo
                    self.engine.execute(query, t_date, t_action, t_price, qty, position, bank, total_value)
                elif cma_act == 0 and frl_act != 1 and frl_k_act != 1 and position != 0:
                    # Set dictionary value for Act to Sell
                    t_action = 'Sell'
                    value = qty * t_price
                    position = 0
                    qty = 0
                    bank = bank + value
                    total_value = bank
                    query = "INSERT INTO %s([tradeDate],[Act], [Price], [Qty], [PositionsHeld], [BuyingPower]," \
                            " [PortfolioValue]) " \
                            "values(?, ?, ?, ?, ?, ?, ?)" % ticker_trades_dbo
                    self.engine.execute(query, t_date, t_action, t_price, qty, position, bank, total_value)
                elif cma_act != 1 and frl_act == 0 and frl_k_act != 1 and position != 0:
                    # Set dictionary value for Act to Sell
                    t_action = 'Sell'
                    value = qty * t_price
                    position = 0
                    qty = 0
                    bank = bank + value
                    total_value = bank
                    query = "INSERT INTO %s([tradeDate],[Act], [Price], [Qty], [PositionsHeld], [BuyingPower]," \
                            " [PortfolioValue]) " \
                            "values(?, ?, ?, ?, ?, ?, ?)" % ticker_trades_dbo
                    self.engine.execute(query, t_date, t_action, t_price, qty, position, bank, total_value)

                elif cma_act == 3 and frl_act == 4 and frl_k_act == 4:

                    if t_date == now:
                        t_action = 'HOLD'
                        # value = qty * t_price
                        total_value = value
                        query = "INSERT INTO %s([tradeDate],[Act], [Price], [Qty], [PositionsHeld], [BuyingPower]," \
                                " [PortfolioValue]) " \
                                "values(?, ?, ?, ?, ?, ?, ?)" % ticker_trades_dbo
                        self.engine.execute(query, t_date, t_action, t_price, qty, position, bank, total_value)
                    else:
                        """
                            No Action
                        """
                # Else Bank is empty or Position is empty
                elif ((cma_act == 0 and frl_act == 0 and frl_k_act == 0)
                      or (cma_act == 0 and frl_act != 1 and frl_k_act != 1)
                      or (cma_act != 1 and frl_act == 0 and frl_k_act != 1)
                      or (cma_act != 1 and frl_act != 1 and frl_k_act == 0)
                      and position == 0) \
                        or \
                        ((cma_act == 1 and frl_act != 0 and frl_k_act != 0)
                         or (cma_act != 0 and frl_act == 1 and frl_k_act != 0)
                         or (cma_act != 0 and frl_act != 0  and frl_k_act == 1)
                         and bank == 0):
                    if t_date == now:
                        t_action = 'HOLD'
                        total_value = value
                        query = "INSERT INTO %s([tradeDate],[Act], [Price], [Qty], [PositionsHeld], [BuyingPower]," \
                                " [PortfolioValue]) " \
                                "values(?, ?, ?, ?, ?, ?, ?)" % ticker_trades_dbo
                        self.engine.execute(query, t_date, t_action, t_price, qty, position, bank, total_value)
                    else:
                        """
                            No Action
                        """
                # End Activity Loop
                x = x + 1
            # End Data Loop
            i = i + 1
