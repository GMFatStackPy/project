import pyodbc as py


class ExecuteSP:
    def exec_sp(self):
        # CHANGE REQUEST CR-2: def exec_sp(self, conn)
        """
        Method to call stored procedures related to the database

        :return: none
        """
        # CR-2
        # NEED TO remove py.connect( "<CONTENTS>" ) and store in TextFile.  Pass as parameter to exec_sp() from calls.
        conn = py.connect("Driver={SQL Server Native Client 11.0};"
                          "Server=localhost;"
                          "Database=GMFSP_db;"
                          "uid=sa;pwd=Gmfintech18", autocommit=True)
        cursor = conn.cursor()
        cursor.execute("{CALL dbo.sp_Pred}")
        conn.close()
