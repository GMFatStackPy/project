USE [GMFSP_db]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_Get_TradeAction]    Script Date: 12/10/2018 10:35:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Create first then Alter
ALTER FUNCTION [dbo].[fn_Get_TradeAction]
(
	-- Add the parameters for the function here
	@closePrice float,
	@FRLLineLong float,
	@LAGclosePrice1 int,
	@LAGclosePrice2 int,
	@LAGclosePrice3 int,
	@LAGclosePrice4 int,
	@LAGclosePrice5 int

)
RETURNS int
AS
BEGIN
	-- Declare the return variable here
	Declare @i int=0
		
	  if (@closePrice <= (@FRLLineLong + (@FRLLineLong * 0.025))
		and @closePrice >= (@FRLLineLong + (@FRLLineLong * 0.015)) 
		and @closePrice < @LAGclosePrice1)
		OR
		(@closePrice <= (@FRLLineLong + (@FRLLineLong * 0.025))
		and @closePrice >= (@FRLLineLong + (@FRLLineLong * 0.015)) 
		and @closePrice < @LAGclosePrice2)
		OR
		(@closePrice <= (@FRLLineLong + (@FRLLineLong * 0.025))
		and @closePrice >= (@FRLLineLong + (@FRLLineLong * 0.015)) 
		and @closePrice < @LAGclosePrice3)
		OR
		(@closePrice <= (@FRLLineLong + (@FRLLineLong * 0.025))
		and @closePrice >= (@FRLLineLong + (@FRLLineLong * 0.015)) 
		and @closePrice < @LAGclosePrice4)
		OR
		(@closePrice <= (@FRLLineLong + (@FRLLineLong * 0.025))
		and @closePrice >= (@FRLLineLong + (@FRLLineLong * 0.015)) 
		and @closePrice < @LAGclosePrice5)

		set @i=1
	

	return @i



END
