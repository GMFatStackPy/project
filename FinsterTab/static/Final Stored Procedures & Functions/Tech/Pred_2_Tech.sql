USE [GMFSP_db]
GO
/****** Object:  StoredProcedure [dbo].[sp_Pred2_Tech]    Script Date: 12/10/2018 10:35:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-----------------------------------------------
-- Author: Chris Del Duco
-- Author: Mohammad Moubadder
--
--			STORED PROCEDURE:
--				sp_Pred2
-----------------------------------------------

ALTER PROCEDURE [dbo].[sp_Pred2_Tech]

AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;

-----------------------------------------------
-- FINANCIAL INSTRUMENT:	S&P 500 ETF (AAPL)
-- COMPUTATIONS:			FRL Predictions
-- CREATES dbo.AAPL_Pred2 from dbo.AAPL_Pred and dbo.AAPL_TS
-----------------------------------------------
IF EXISTS (SELECT * 
	FROM GMFSP_db.INFORMATION_SCHEMA.TABLES   
	WHERE TABLE_SCHEMA = N'dbo'  AND TABLE_NAME = N'AAPL_Pred2')
Drop table dbo.AAPL_Pred2
SELECT tick.tradeDate, tick.closePrice, tick.momentumA, tick.momentumB, ts.sPeak, ts.sTrough,
	--If the momentum of the most recent day is less than the 2nd to last day then predict a drop for the next day, reverse is true
	(CASE WHEN LAG(momentumA,0) OVER (ORDER BY tick.tradeDate) < LAG(momentumA,1) OVER (ORDER BY tick.tradeDate)
			AND LAG(momentumA,0) OVER (ORDER BY tick.tradeDate) < 0
			OR LAG(momentumA,0) OVER (ORDER BY tick.tradeDate) < LAG(momentumA,1) OVER (ORDER BY tick.tradeDate)
			AND LAG(momentumA,0) OVER (ORDER BY tick.tradeDate) > 0
	THEN 0
	ELSE
		CASE WHEN LAG(momentumA,0) OVER (ORDER BY tick.tradeDate) > LAG(momentumA,1) OVER (ORDER BY tick.tradeDate)
			--	AND LAG(momentumA,0) OVER (ORDER BY tick.tradeDate) > 0
		THEN 1
		END
	END) AS predNextDay,
	
	-------------------------------------
	--Pos and Neg momentum affects the outcome of frl lines
	-------------------------------------
	(CASE WHEN momentumA < 0.00
		THEN sPeak - ((sPeak - sTrough) * 1.236) 
	END)AS fibExtHighNeg,
	(Case WHEN momentumA < 0.00
		THEN sPeak - ((sPeak - sTrough) * 1.382)
	END) AS fibExtLowNeg,
	--Fib Extension with a positive momentum
	(CASE WHEN momentumA > 0.00
		THEN sPeak + ((sPeak - sTrough) * 1.236)
	END) AS fibExtHighPos,
	(CASE WHEN momentumA > 0.00
		THEN sPeak + ((sPeak - sTrough) * 1.382)
	END) AS fibExtLowPos,
		
	--12 Day Osc
	--(((LAG(closePrice,0) OVER (ORDER BY tick.tradeDate) - troughShort)/ (peakShort - troughShort)) * 100) AS Oscillator
	--100 Day Osc
	(((LAG(closePrice,0) OVER (ORDER BY tick.tradeDate) - lTrough)/ (lPeak - lTrough)) * 100) AS hunOscillator,
	--1k Day Osc
	(((LAG(closePrice,0) OVER (ORDER BY tick.tradeDate) - kTrough)/ (kPeak - kTrough)) * 100) AS oneKOscillator
	
INTO dbo.AAPL_Pred2
FROM dbo.AAPL_Pred AS tick
JOIN dbo.AAPL_TS as ts on ts.tradeDate = tick.tradeDate
ORDER BY tick.tradeDate, ts.tradeDate ASC

-----------------------------------------------
-- FINANCIAL INSTRUMENT:	AA S&P Pharma ETF (FB)
-- COMPUTATIONS:			FRL Predictions
-- CREATES dbo.FB_Pred2 from dbo.FB_Pred and dbo.FB_TS
-----------------------------------------------
IF EXISTS (SELECT * 
	FROM GMFSP_db.INFORMATION_SCHEMA.TABLES   
	WHERE TABLE_SCHEMA = N'dbo'  AND TABLE_NAME = N'FB_Pred2')
Drop table dbo.FB_Pred2
SELECT tick.tradeDate, tick.closePrice, tick.momentumA, tick.momentumB, ts.sPeak, ts.sTrough,
	--If the momentum of the most recent day is less than the 2nd to last day then predict a drop for the next day, reverse is true
	(CASE WHEN LAG(momentumA,0) OVER (ORDER BY tick.tradeDate) < LAG(momentumA,1) OVER (ORDER BY tick.tradeDate)
			AND LAG(momentumA,0) OVER (ORDER BY tick.tradeDate) < 0
			OR LAG(momentumA,0) OVER (ORDER BY tick.tradeDate) < LAG(momentumA,1) OVER (ORDER BY tick.tradeDate)
			AND LAG(momentumA,0) OVER (ORDER BY tick.tradeDate) > 0
	THEN 0
	ELSE
		CASE WHEN LAG(momentumA,0) OVER (ORDER BY tick.tradeDate) > LAG(momentumA,1) OVER (ORDER BY tick.tradeDate)
			--	AND LAG(momentumA,0) OVER (ORDER BY tick.tradeDate) > 0
		THEN 1
		END
	END) AS predNextDay,
	
	-------------------------------------
	--Pos and Neg momentum affects the outcome of frl lines
	-------------------------------------
	(CASE WHEN momentumA < 0.00
		THEN sPeak - ((sPeak - sTrough) * 1.236) 
	END)AS fibExtHighNeg,
	(Case WHEN momentumA < 0.00
		THEN sPeak - ((sPeak - sTrough) * 1.382)
	END) AS fibExtLowNeg,
	--Fib Extension with a positive momentum
	(CASE WHEN momentumA > 0.00
		THEN sPeak + ((sPeak - sTrough) * 1.236)
	END) AS fibExtHighPos,
	(CASE WHEN momentumA > 0.00
		THEN sPeak + ((sPeak - sTrough) * 1.382)
	END) AS fibExtLowPos,
		
	--12 Day Osc
	--(((LAG(closePrice,0) OVER (ORDER BY tick.tradeDate) - troughShort)/ (peakShort - troughShort)) * 100) AS Oscillator
	--100 Day Osc
	(((LAG(closePrice,0) OVER (ORDER BY tick.tradeDate) - lTrough)/ (lPeak - lTrough)) * 100) AS hunOscillator,
	--1k Day Osc
	(((LAG(closePrice,0) OVER (ORDER BY tick.tradeDate) - kTrough)/ (kPeak - kTrough)) * 100) AS oneKOscillator
	
INTO dbo.FB_Pred2
FROM dbo.FB_Pred AS tick
JOIN dbo.FB_TS as ts on ts.tradeDate = tick.tradeDate
ORDER BY tick.tradeDate, ts.tradeDate ASC

-----------------------------------------------
-- FINANCIAL INSTRUMENT:	GM (GM)
-- COMPUTATIONS:			FRL Predictions
-- CREATES dbo.GOOG_Pred2 from dbo.GOOG_Pred and dbo.GOOG_TS
-----------------------------------------------
IF EXISTS (SELECT * 
	FROM GMFSP_db.INFORMATION_SCHEMA.TABLES   
	WHERE TABLE_SCHEMA = N'dbo'  AND TABLE_NAME = N'GOOG_Pred2')
Drop table dbo.GOOG_Pred2
SELECT tick.tradeDate, tick.closePrice, tick.momentumA, tick.momentumB, ts.sPeak, ts.sTrough,
	--If the momentum of the most recent day is less than the 2nd to last day then predict a drop for the next day, reverse is true
	(CASE WHEN LAG(momentumA,0) OVER (ORDER BY tick.tradeDate) < LAG(momentumA,1) OVER (ORDER BY tick.tradeDate)
			AND LAG(momentumA,0) OVER (ORDER BY tick.tradeDate) < 0
			OR LAG(momentumA,0) OVER (ORDER BY tick.tradeDate) < LAG(momentumA,1) OVER (ORDER BY tick.tradeDate)
			AND LAG(momentumA,0) OVER (ORDER BY tick.tradeDate) > 0
	THEN 0
	ELSE
		CASE WHEN LAG(momentumA,0) OVER (ORDER BY tick.tradeDate) > LAG(momentumA,1) OVER (ORDER BY tick.tradeDate)
			--	AND LAG(momentumA,0) OVER (ORDER BY tick.tradeDate) > 0
		THEN 1
		END
	END) AS predNextDay,
	
	-------------------------------------
	--Pos and Neg momentum affects the outcome of frl lines
	-------------------------------------
	(CASE WHEN momentumA < 0.00
		THEN sPeak - ((sPeak - sTrough) * 1.236) 
	END)AS fibExtHighNeg,
	(Case WHEN momentumA < 0.00
		THEN sPeak - ((sPeak - sTrough) * 1.382)
	END) AS fibExtLowNeg,
	--Fib Extension with a positive momentum
	(CASE WHEN momentumA > 0.00
		THEN sPeak + ((sPeak - sTrough) * 1.236)
	END) AS fibExtHighPos,
	(CASE WHEN momentumA > 0.00
		THEN sPeak + ((sPeak - sTrough) * 1.382)
	END) AS fibExtLowPos,
		
	--12 Day Osc
	--(((LAG(closePrice,0) OVER (ORDER BY tick.tradeDate) - troughShort)/ (peakShort - troughShort)) * 100) AS Oscillator
	--100 Day Osc
	(((LAG(closePrice,0) OVER (ORDER BY tick.tradeDate) - lTrough)/ (lPeak - lTrough)) * 100) AS hunOscillator,
	--1k Day Osc
	(((LAG(closePrice,0) OVER (ORDER BY tick.tradeDate) - kTrough)/ (kPeak - kTrough)) * 100) AS oneKOscillator
	
INTO dbo.GOOG_Pred2
FROM dbo.GOOG_Pred AS tick
JOIN dbo.GOOG_TS as ts on ts.tradeDate = tick.tradeDate
ORDER BY tick.tradeDate, ts.tradeDate ASC

BEGIN
	exec dbo.sp_CMA_FRL_ACT_Tech;
END

END
