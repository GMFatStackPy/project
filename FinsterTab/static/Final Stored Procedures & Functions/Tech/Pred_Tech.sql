USE [GMFSP_db]
GO
/****** Object:  StoredProcedure [dbo].[sp_Pred_Tech]    Script Date: 12/10/2018 10:34:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-----------------------------------------------
-- Author: Chris Del Duco
-- Author: Mohammad Moubadder
--
--			STORED PROCEDURE:
--				sp_Pred_Tech
-----------------------------------------------

ALTER PROCEDURE [dbo].[sp_Pred_Tech]

AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;


-----------------------------------------------
-- FINANCIAL INSTRUMENT:	S&P 500 ETF Trust (AAPL)
-- COMPUTATIONS:			CMA
-- CREATES dbo.AAPL_Pred from dbo.AAPL
-----------------------------------------------
IF EXISTS (SELECT * 
	FROM GMFSP_db.INFORMATION_SCHEMA.TABLES   
	WHERE TABLE_SCHEMA = N'dbo'  AND TABLE_NAME = N'AAPL_Pred')
Drop table dbo.AAPL_Pred
SELECT tick.tradeDate, tick.closePrice,

	(LAG(wCMA, 1) OVER (ORDER BY ts.tradeDate) / lCMA) as BUYweekApproach,
	(wCMA / lCMA) as week_Long,
	(LAG(wCMA, 1) OVER (ORDER BY ts.tradeDate) / sCMA) as SELLweekApproach,
	(wCMA / sCMA) as week_Short,
		
	--100 day FRL Lines
	lPeak - ((lPeak - lTrough) * 0.236) AS highFRLLineLong,
	lPeak - ((lPeak - lTrough) * 0.382) AS medFRLLineLong,
	lPeak - ((lPeak - lTrough) * 0.618) AS lowFRLLineLong,
	--1000 day FRL lines
	kPeak - ((kPeak - kTrough) * 0.236) AS highFRLLineOneK,
	kPeak - ((kPeak - kTrough) * 0.382) AS medFRLLineOneK,
	kPeak - ((kPeak - kTrough ) * 0.618) AS lowFRLLineOneK,
	
	closePrice / LAG(closePrice,1) OVER (ORDER BY tick.tradeDate) AS ActualChange,
	-- 5 day Momentum
	closePrice / LAG(closePrice,5) OVER (ORDER BY ts.tradeDate) AS momentumA,
	-- 10 day Momentum
	closePrice / LAG(closePrice,10) OVER (ORDER BY ts.tradeDate) AS momentumB,
	(closePrice - LAG(closePrice,5) OVER (ORDER BY ts.tradeDate)) / LAG(closePrice,5) OVER (ORDER BY ts.tradeDate) * 100 AS rateOfChange

INTO dbo.AAPL_Pred
FROM dbo.AAPL AS tick
JOIN dbo.AAPL_TS as ts on ts.tradeDate = tick.tradeDate
ORDER BY tick.tradeDate, ts.tradeDate ASC

-----------------------------------------------
-- FINANCIAL INSTRUMENT:	S&P Pharm ETF (FB)
-- COMPUTATIONS:			CMA
-- CREATES dbo.FB_Pred from dbo.FB
-----------------------------------------------
IF EXISTS (SELECT * 
	FROM GMFSP_db.INFORMATION_SCHEMA.TABLES   
	WHERE TABLE_SCHEMA = N'dbo'  AND TABLE_NAME = N'FB_Pred')
Drop table dbo.FB_Pred
SELECT tick.tradeDate, tick.closePrice,

	(LAG(wCMA, 1) OVER (ORDER BY ts.tradeDate) / lCMA) as BUYweekApproach,
	(wCMA / lCMA) as week_Long,
	(LAG(wCMA, 1) OVER (ORDER BY ts.tradeDate) / sCMA) as SELLweekApproach,
	(wCMA / sCMA) as week_Short,
		
	--100 day FRL Lines
	lPeak - ((lPeak - lTrough) * 0.236) AS highFRLLineLong,
	lPeak - ((lPeak - lTrough) * 0.382) AS medFRLLineLong,
	lPeak - ((lPeak - lTrough) * 0.618) AS lowFRLLineLong,
	--1000 day FRL lines
	kPeak - ((kPeak - kTrough) * 0.236) AS highFRLLineOneK,
	kPeak - ((kPeak - kTrough) * 0.382) AS medFRLLineOneK,
	kPeak - ((kPeak - kTrough ) * 0.618) AS lowFRLLineOneK,
	
	closePrice / LAG(closePrice,1) OVER (ORDER BY tick.tradeDate) AS ActualChange,
	-- 5 day Momentum
	closePrice / LAG(closePrice,5) OVER (ORDER BY ts.tradeDate) AS momentumA,
	-- 10 day Momentum
	closePrice / LAG(closePrice,10) OVER (ORDER BY ts.tradeDate) AS momentumB,
	(closePrice - LAG(closePrice,5) OVER (ORDER BY ts.tradeDate)) / LAG(closePrice,5) OVER (ORDER BY ts.tradeDate) * 100 AS rateOfChange

INTO dbo.FB_Pred
FROM dbo.FB AS tick
JOIN dbo.FB_TS as ts on ts.tradeDate = tick.tradeDate
ORDER BY tick.tradeDate, ts.tradeDate ASC

----------------------------------------
-- FINANCIAL INSTRUMENT:	PFIZER (GOOG)
-- COMPUTATIONS:			CMA
-- CREATES dbo.GOOG_Pred from dbo.GOOG
----------------------------------------	
IF EXISTS (SELECT * 
	FROM GMFSP_db.INFORMATION_SCHEMA.TABLES   
	WHERE TABLE_SCHEMA = N'dbo'  AND TABLE_NAME = N'GOOG_Pred')
Drop table dbo.GOOG_Pred
SELECT tick.tradeDate, tick.closePrice,

	(LAG(wCMA, 1) OVER (ORDER BY ts.tradeDate) / lCMA) as BUYweekApproach,
	(wCMA / lCMA) as week_Long,
	(LAG(wCMA, 1) OVER (ORDER BY ts.tradeDate) / sCMA) as SELLweekApproach,
	(wCMA / sCMA) as week_Short,
		
	--100 day FRL Lines
	lPeak - ((lPeak - lTrough) * 0.236) AS highFRLLineLong,
	lPeak - ((lPeak - lTrough) * 0.382) AS medFRLLineLong,
	lPeak - ((lPeak - lTrough) * 0.618) AS lowFRLLineLong,
	--1000 day FRL lines
	kPeak - ((kPeak - kTrough) * 0.236) AS highFRLLineOneK,
	kPeak - ((kPeak - kTrough) * 0.382) AS medFRLLineOneK,
	kPeak - ((kPeak - kTrough ) * 0.618) AS lowFRLLineOneK,
	
	closePrice / LAG(closePrice,1) OVER (ORDER BY tick.tradeDate) AS ActualChange,
	-- 5 day Momentum
	closePrice / LAG(closePrice,5) OVER (ORDER BY ts.tradeDate) AS momentumA,
	-- 10 day Momentum
	closePrice / LAG(closePrice,10) OVER (ORDER BY ts.tradeDate) AS momentumB,
	(closePrice - LAG(closePrice,5) OVER (ORDER BY ts.tradeDate)) / LAG(closePrice,5) OVER (ORDER BY ts.tradeDate) * 100 AS rateOfChange

INTO dbo.GOOG_Pred
FROM dbo.GOOG AS tick
JOIN dbo.GOOG_TS as ts on ts.tradeDate = tick.tradeDate
ORDER BY tick.tradeDate, ts.tradeDate ASC


BEGIN
	exec dbo.sp_Pred2_Tech;
END

END
