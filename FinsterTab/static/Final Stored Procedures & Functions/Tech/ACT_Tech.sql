USE [GMFSP_db]
GO
/****** Object:  StoredProcedure [dbo].[sp_CMA_FRL_ACT_Tech]    Script Date: 12/10/2018 10:32:29 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-----------------------------------------------
-- Author: Chris Del Duco
-- Author: Mohammad Moubadder
--
--			STORED PROCEDURE:
--			 sp_CMA_FRL_ACT_Auto
-----------------------------------------------

ALTER PROCEDURE [dbo].[sp_CMA_FRL_ACT_Tech]
AS
BEGIN
-----------------------------------------------
-- Create date: 11/1/2018
-- Description:	
---------------------------------------------------
-- FINANCIAL INSTRUMENT:	S&P 500 ETF Trust (F)
-- BUY SELL HOLD:			CASE LOGIC
-- CREATES dbo.AAPL_Act from AAPL_Pred
---------------------------------------------------
SET NOCOUNT ON;

IF EXISTS (SELECT * 
	FROM GMFSP_db.INFORMATION_SCHEMA.TABLES   
	WHERE TABLE_SCHEMA = N'dbo'  AND TABLE_NAME = N'AAPL_Act')
Drop table dbo.AAPL_Act

--select * from dbo.AAPL_Act
--truncate table dbo.AAPL_Act


SELECT p.tradeDate, p.closePrice,
	(CASE WHEN lCMA is NULL
		THEN 3
		ELSE
		-- Yesterday's wCMA is higher than lCMA and approaches the lCMA "GOLDEN CROSS" or Stock is lower than expected
		CASE WHEN 
			-- Yesterdays wCMA is Higher than lCMA, but not Drastically
			BUYweekApproach > 1.015 and BUYweekApproach < 1.065
			-- Todays wCMA is closer than yesterday
			and week_Long >= 1.015 
			-- Yesterdays closePrice 5 days ago is higher than todays
			and p.momentumA < .98
			THEN 1
			ELSE
			-- Yesterday's wCMA is higher than sCMA and is approaching the "DEATH CROSS" or Stock is Inflated, so CASH IN
			CASE WHEN 
			-- Yesterday's wCMA is Higher than today's sCMA
			SELLweekApproach > 1.006
			-- and Todays wCMA is closer than yesterday
			and week_Short >= .95
			-- and Yesterdays sCMA is higher than todays
			and (LAG(sCMA, 3) OVER (ORDER BY ts.tradeDate) - sCMA) > 0 
			THEN 0
				ELSE 3 --  Too Volatile
			END
		END
	END) AS CMATradeAction,

(Case when dbo.fn_Get_TradeAction(p.closePrice,highFRLLineLong,LAG(p.closePrice,1) OVER (ORDER BY p.tradeDate)
	                                                        ,LAG(p.closePrice,2) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,3) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,4) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,5) OVER (ORDER BY p.tradeDate))=1
	OR dbo.fn_Get_TradeAction(p.closePrice,medFRLLineLong,LAG(p.closePrice,1) OVER (ORDER BY p.tradeDate)
	                                                        ,LAG(p.closePrice,2) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,3) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,4) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,5) OVER (ORDER BY p.tradeDate))=1
	or dbo.fn_Get_TradeAction(p.closePrice,lowFRLLineLong,LAG(p.closePrice,1) OVER (ORDER BY p.tradeDate)
	                                                        ,LAG(p.closePrice,2) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,3) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,4) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,5) OVER (ORDER BY p.tradeDate))=1
															
	THEN 4
	ELSE
	CASE WHEN (p.tradeDate <= DATEADD(month, -6, GETDATE()))
		THEN 
		CASE WHEN 
		p.closePrice <= (lowFRLLineLong * .95)
		and p.momentumA < .947
		--p.closePrice <= (highFRLLineLong + 0.5)
		--and p.closePrice >= (highFRLLineLong - 0.35)
		--or p.closePrice <= (medFRLLineLong + 0.5)
		--and p.closePrice >= (medFRLLineLong - 0.35)
		--or p.closePrice <= (lowFRLLineLong + 0.5)
		--and p.closePrice >= (lowFRLLineLong - 0.35)
			THEN 1
			ELSE
			CASE WHEN 
			p.closePrice >= (highFRLLineLong * .93)
			and p.momentumA > .955
			and (LAG(p.ActualChange, 1) OVER (ORDER BY p.tradeDate) - p.ActualChange) < 0.1
			--p.closePrice <= (highFRLLineOneK + (highFRLLineOneK * 0.0225))
			--and p.closePrice >= (highFRLLineOneK + (highFRLLineOneK * 0.0125))
			--or p.closePrice <= (medFRLLineOneK + (medFRLLineOneK * 0.0225))
			--and p.closePrice >= (medFRLLineOneK + (medFRLLineOneK * 0.0125))
			--or p.closePrice <= (lowFRLLineOneK + (lowFRLLineOneK * 0.0225))
			--and p.closePrice >= (lowFRLLineOneK + (lowFRLLineOneK * 0.0125))
				THEN 0
				ELSE 4
			END
		END
		ELSE
		CASE WHEN 
		p.closePrice <= (medFRLLineLong * .96)
		and p.momentumA < .947
		--p.closePrice <= (highFRLLineLong + 0.5)
		--and p.closePrice >= (highFRLLineLong - 0.35)
		--or p.closePrice <= (medFRLLineLong + 0.5)
		--and p.closePrice >= (medFRLLineLong - 0.35)
		--or p.closePrice <= (lowFRLLineLong + 0.5)
		--and p.closePrice >= (lowFRLLineLong - 0.35)
			THEN 1
			ELSE
			CASE WHEN 
			p.closePrice >= (highFRLLineLong * .96)
			and p.momentumA > .96
			and (LAG(p.ActualChange, 1) OVER (ORDER BY p.tradeDate) - p.ActualChange) < 0.1
			--p.closePrice <= (highFRLLineOneK + (highFRLLineOneK * 0.0225))
			--and p.closePrice >= (highFRLLineOneK + (highFRLLineOneK * 0.0125))
			--or p.closePrice <= (medFRLLineOneK + (medFRLLineOneK * 0.0225))
			--and p.closePrice >= (medFRLLineOneK + (medFRLLineOneK * 0.0125))
			--or p.closePrice <= (lowFRLLineOneK + (lowFRLLineOneK * 0.0225))
			--and p.closePrice >= (lowFRLLineOneK + (lowFRLLineOneK * 0.0125))
				THEN 0
				ELSE 4
			END
		END
	END
END) AS FRLTradeAction,

-----------------------------------------------------
--1000 day
-----------------------------------------------------
(Case when dbo.fn_Get_TradeAction(p.closePrice,highFRLLineOneK,LAG(p.closePrice,1) OVER (ORDER BY p.tradeDate)
	                                                        ,LAG(p.closePrice,2) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,3) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,4) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,5) OVER (ORDER BY p.tradeDate))=1
	OR dbo.fn_Get_TradeAction(p.closePrice,medFRLLineOneK,LAG(p.closePrice,1) OVER (ORDER BY p.tradeDate)
	                                                        ,LAG(p.closePrice,2) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,3) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,4) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,5) OVER (ORDER BY p.tradeDate))=1
	or dbo.fn_Get_TradeAction(p.closePrice,lowFRLLineOneK,LAG(p.closePrice,1) OVER (ORDER BY p.tradeDate)
	                                                        ,LAG(p.closePrice,2) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,3) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,4) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,5) OVER (ORDER BY p.tradeDate))=1
	THEN 4
	ELSE
	CASE WHEN (p.tradeDate <= DATEADD(month, -6, GETDATE()))
	THEN 
		CASE WHEN p.closePrice <= (highFRLLineOneK * 1.01)
		and p.closePrice >= (highFRLLineOneK * .95)
		THEN 1
			ELSE
			CASE WHEN
			-- closePrice is higher than FRL, but not drastically
			p.closePrice >= (highFRLLineOneK * 1.005)
			and p.closePrice < (highFRLLineOneK * 1.04)
				THEN 0
				ELSE 4
			END
		END
	ELSE
		CASE WHEN p.closePrice <= (highFRLLineOneK * 1.03)
		and p.closePrice >= (highFRLLineOneK * 1.005)
		THEN 1
			ELSE
			CASE WHEN
			-- closePrice is higher than FRL, but not drastically
			p.closePrice < (highFRLLineOneK * 1.075)
			and p.closePrice >= (highFRLLineOneK * 1.025)
				THEN 0
				ELSE 4
			END
		END
	END
END) AS FRL_1K_TradeAction

INTO dbo.AAPL_Act
FROM dbo.AAPL_Pred as p
JOIN dbo.AAPL_TS as ts on ts.tradeDate = p.tradeDate
JOIN dbo.AAPL_Pred2 as p2 on p2.tradeDate = p.tradeDate
ORDER BY p.tradeDate, p2.tradeDate, ts.tradeDate ASC


--------------------------- update fib columns F------------------
DECLARE @fibExtLowNeg Float
DECLARE @fibExtHighNeg Float
DECLARE @fibExtLowPos Float
DECLARE @fibExtHighPos Float

SELECT TOP 1 @fibExtLowNeg = fibExtLowNeg
    , @fibExtHighNeg = [fibExtHighNeg]
    , @fibExtLowPos = [fibExtLowPos] 
    ,@fibExtHighPos = [fibExtHighPos]
FROM dbo.AAPL_Pred2 ORDER BY tradeDate DESC
UPDATE dbo.AAPL_Pred2 SET [fibExtLowNeg] = @fibExtLowNeg , [fibExtHighNeg] = @fibExtHighNeg ,  fibExtHighPos = NULL, fibExtLowPos = NULL
--------------------------- update fib columns F------------------

	
-----------------------------------------------
-- FINANCIAL INSTRUMENT:	S&P Pharm ETF (FB)
-- BUY SELL HOLD:			CASE LOGIC
-- CREATES dbo.FB_Act from FB_Pred
-----------------------------------------------
IF EXISTS (SELECT * 
	FROM GMFSP_db.INFORMATION_SCHEMA.TABLES   
	WHERE TABLE_SCHEMA = N'dbo'  AND TABLE_NAME = N'FB_Act')
Drop table dbo.FB_Act


SELECT p.tradeDate, p.closePrice,
	(CASE WHEN lCMA is NULL
		THEN 3
		ELSE
		-- Yesterday's wCMA is higher than lCMA and approaches the lCMA "GOLDEN CROSS" or Stock is lower than expected
		CASE WHEN 
			-- Yesterdays wCMA is Higher than lCMA, but not Drastically
			BUYweekApproach > 1.015 and BUYweekApproach < 1.065
			-- Todays wCMA is closer than yesterday
			and week_Long >= 1.015 
			-- Yesterdays closePrice 5 days ago is higher than todays
			and p.momentumA < .98
			THEN 1
			ELSE
			-- Yesterday's wCMA is higher than sCMA and is approaching the "DEATH CROSS" or Stock is Inflated, so CASH IN
			CASE WHEN 
			-- Yesterday's wCMA is Higher than today's sCMA
			SELLweekApproach > 1.006
			-- and Todays wCMA is closer than yesterday
			and week_Short >= .95
			-- and Yesterdays sCMA is higher than todays
			and (LAG(sCMA, 3) OVER (ORDER BY ts.tradeDate) - sCMA) > 0 
			THEN 0
				ELSE 3 --  Too Volatile
			END
		END
	END) AS CMATradeAction,

(Case when dbo.fn_Get_TradeAction(p.closePrice,highFRLLineLong,LAG(p.closePrice,1) OVER (ORDER BY p.tradeDate)
	                                                        ,LAG(p.closePrice,2) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,3) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,4) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,5) OVER (ORDER BY p.tradeDate))=1
	OR dbo.fn_Get_TradeAction(p.closePrice,medFRLLineLong,LAG(p.closePrice,1) OVER (ORDER BY p.tradeDate)
	                                                        ,LAG(p.closePrice,2) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,3) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,4) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,5) OVER (ORDER BY p.tradeDate))=1
	or dbo.fn_Get_TradeAction(p.closePrice,lowFRLLineLong,LAG(p.closePrice,1) OVER (ORDER BY p.tradeDate)
	                                                        ,LAG(p.closePrice,2) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,3) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,4) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,5) OVER (ORDER BY p.tradeDate))=1
															
	THEN 4
	ELSE
	CASE WHEN (p.tradeDate <= DATEADD(month, -6, GETDATE()))
		THEN 
		CASE WHEN 
		p.closePrice <= (lowFRLLineLong * .95)
		and p.momentumA < .947
		--p.closePrice <= (highFRLLineLong + 0.5)
		--and p.closePrice >= (highFRLLineLong - 0.35)
		--or p.closePrice <= (medFRLLineLong + 0.5)
		--and p.closePrice >= (medFRLLineLong - 0.35)
		--or p.closePrice <= (lowFRLLineLong + 0.5)
		--and p.closePrice >= (lowFRLLineLong - 0.35)
			THEN 1
			ELSE
			CASE WHEN 
			p.closePrice >= (highFRLLineLong * .93)
			and p.momentumA > .955
			and (LAG(p.ActualChange, 1) OVER (ORDER BY p.tradeDate) - p.ActualChange) < 0.1
			--p.closePrice <= (highFRLLineOneK + (highFRLLineOneK * 0.0225))
			--and p.closePrice >= (highFRLLineOneK + (highFRLLineOneK * 0.0125))
			--or p.closePrice <= (medFRLLineOneK + (medFRLLineOneK * 0.0225))
			--and p.closePrice >= (medFRLLineOneK + (medFRLLineOneK * 0.0125))
			--or p.closePrice <= (lowFRLLineOneK + (lowFRLLineOneK * 0.0225))
			--and p.closePrice >= (lowFRLLineOneK + (lowFRLLineOneK * 0.0125))
				THEN 0
				ELSE 4
			END
		END
		ELSE
		CASE WHEN 
		p.closePrice <= (medFRLLineLong * .96)
		and p.momentumA < .947
		--p.closePrice <= (highFRLLineLong + 0.5)
		--and p.closePrice >= (highFRLLineLong - 0.35)
		--or p.closePrice <= (medFRLLineLong + 0.5)
		--and p.closePrice >= (medFRLLineLong - 0.35)
		--or p.closePrice <= (lowFRLLineLong + 0.5)
		--and p.closePrice >= (lowFRLLineLong - 0.35)
			THEN 1
			ELSE
			CASE WHEN 
			p.closePrice >= (highFRLLineLong * .96)
			and p.momentumA > .96
			and (LAG(p.ActualChange, 1) OVER (ORDER BY p.tradeDate) - p.ActualChange) < 0.1
			--p.closePrice <= (highFRLLineOneK + (highFRLLineOneK * 0.0225))
			--and p.closePrice >= (highFRLLineOneK + (highFRLLineOneK * 0.0125))
			--or p.closePrice <= (medFRLLineOneK + (medFRLLineOneK * 0.0225))
			--and p.closePrice >= (medFRLLineOneK + (medFRLLineOneK * 0.0125))
			--or p.closePrice <= (lowFRLLineOneK + (lowFRLLineOneK * 0.0225))
			--and p.closePrice >= (lowFRLLineOneK + (lowFRLLineOneK * 0.0125))
				THEN 0
				ELSE 4
			END
		END
	END
END) AS FRLTradeAction,

-----------------------------------------------------
--1000 day
-----------------------------------------------------
(Case when dbo.fn_Get_TradeAction(p.closePrice,highFRLLineOneK,LAG(p.closePrice,1) OVER (ORDER BY p.tradeDate)
	                                                        ,LAG(p.closePrice,2) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,3) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,4) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,5) OVER (ORDER BY p.tradeDate))=1
	OR dbo.fn_Get_TradeAction(p.closePrice,medFRLLineOneK,LAG(p.closePrice,1) OVER (ORDER BY p.tradeDate)
	                                                        ,LAG(p.closePrice,2) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,3) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,4) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,5) OVER (ORDER BY p.tradeDate))=1
	or dbo.fn_Get_TradeAction(p.closePrice,lowFRLLineOneK,LAG(p.closePrice,1) OVER (ORDER BY p.tradeDate)
	                                                        ,LAG(p.closePrice,2) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,3) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,4) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,5) OVER (ORDER BY p.tradeDate))=1
	THEN 4
	ELSE
	CASE WHEN (p.tradeDate <= DATEADD(month, -6, GETDATE()))
	THEN 
		CASE WHEN p.closePrice <= (highFRLLineOneK * 1.01)
		and p.closePrice >= (highFRLLineOneK * .95)
		THEN 1
			ELSE
			CASE WHEN
			-- closePrice is higher than FRL, but not drastically
			p.closePrice >= (highFRLLineOneK * 1.005)
			and p.closePrice < (highFRLLineOneK * 1.04)
				THEN 0
				ELSE 4
			END
		END
	ELSE
		CASE WHEN p.closePrice <= (highFRLLineOneK * 1.03)
		and p.closePrice >= (highFRLLineOneK * 1.005)
		THEN 1
			ELSE
			CASE WHEN
			-- closePrice is higher than FRL, but not drastically
			p.closePrice < (highFRLLineOneK * 1.075)
			and p.closePrice >= (highFRLLineOneK * 1.025)
				THEN 0
				ELSE 4
			END
		END
	END
END) AS FRL_1K_TradeAction

INTO dbo.FB_Act
FROM dbo.FB_Pred as p
JOIN dbo.FB_TS as ts on ts.tradeDate = p.tradeDate
JOIN dbo.FB_Pred2 as p2 on p2.tradeDate = p.tradeDate
ORDER BY p.tradeDate, p2.tradeDate, ts.tradeDate ASC

	
--------------------------- update fib columns FB------------------
SELECT TOP 1 @fibExtLowNeg = fibExtLowNeg
    , @fibExtHighNeg = [fibExtHighNeg]
    , @fibExtLowPos = [fibExtLowPos] 
    ,@fibExtHighPos = [fibExtHighPos]
FROM dbo.FB_Pred2 ORDER BY tradeDate DESC
UPDATE dbo.FB_Pred2 SET [fibExtLowNeg] = @fibExtLowNeg , [fibExtHighNeg] = @fibExtHighNeg ,  fibExtHighPos = NULL, fibExtLowPos = NULL
--------------------------- update fib columns FB------------------

----------------------------------------
-- FINANCIAL INSTRUMENT:	PFIZER (GOOG)
-- BUY SELL HOLD:			CASE LOGIC
-- CREATES dbo.GOOG_Act from GOOG_Pred
----------------------------------------
IF EXISTS (SELECT * 
	FROM GMFSP_db.INFORMATION_SCHEMA.TABLES   
	WHERE TABLE_SCHEMA = N'dbo'  AND TABLE_NAME = N'GOOG_Act')
drop table dbo.GOOG_Act


SELECT p.tradeDate, p.closePrice,
	(CASE WHEN lCMA is NULL
		THEN 3
		ELSE
		-- Yesterday's wCMA is higher than lCMA and approaches the lCMA "GOLDEN CROSS" or Stock is lower than expected
		CASE WHEN 
			-- Yesterdays wCMA is Higher than lCMA, but not Drastically
			BUYweekApproach > 1.015 and BUYweekApproach < 1.065
			-- Todays wCMA is closer than yesterday
			and week_Long >= 1.015 
			-- Yesterdays closePrice 5 days ago is higher than todays
			and p.momentumA < .98
			THEN 1
			ELSE
			-- Yesterday's wCMA is higher than sCMA and is approaching the "DEATH CROSS" or Stock is Inflated, so CASH IN
			CASE WHEN 
			-- Yesterday's wCMA is Higher than today's sCMA
			SELLweekApproach > 1.006
			-- and Todays wCMA is closer than yesterday
			and week_Short >= .95
			-- and Yesterdays sCMA is higher than todays
			and (LAG(sCMA, 3) OVER (ORDER BY ts.tradeDate) - sCMA) > 0 
			THEN 0
				ELSE 3 --  Too Volatile
			END
		END
	END) AS CMATradeAction,

(Case when dbo.fn_Get_TradeAction(p.closePrice,highFRLLineLong,LAG(p.closePrice,1) OVER (ORDER BY p.tradeDate)
	                                                        ,LAG(p.closePrice,2) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,3) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,4) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,5) OVER (ORDER BY p.tradeDate))=1
	OR dbo.fn_Get_TradeAction(p.closePrice,medFRLLineLong,LAG(p.closePrice,1) OVER (ORDER BY p.tradeDate)
	                                                        ,LAG(p.closePrice,2) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,3) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,4) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,5) OVER (ORDER BY p.tradeDate))=1
	or dbo.fn_Get_TradeAction(p.closePrice,lowFRLLineLong,LAG(p.closePrice,1) OVER (ORDER BY p.tradeDate)
	                                                        ,LAG(p.closePrice,2) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,3) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,4) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,5) OVER (ORDER BY p.tradeDate))=1
															
	THEN 4
	ELSE
	CASE WHEN (p.tradeDate <= DATEADD(month, -6, GETDATE()))
		THEN 
		CASE WHEN 
		p.closePrice <= (lowFRLLineLong * .95)
		and p.momentumA < .947
		--p.closePrice <= (highFRLLineLong + 0.5)
		--and p.closePrice >= (highFRLLineLong - 0.35)
		--or p.closePrice <= (medFRLLineLong + 0.5)
		--and p.closePrice >= (medFRLLineLong - 0.35)
		--or p.closePrice <= (lowFRLLineLong + 0.5)
		--and p.closePrice >= (lowFRLLineLong - 0.35)
			THEN 1
			ELSE
			CASE WHEN 
			p.closePrice >= (highFRLLineLong * .93)
			and p.momentumA > .955
			and (LAG(p.ActualChange, 1) OVER (ORDER BY p.tradeDate) - p.ActualChange) < 0.1
			--p.closePrice <= (highFRLLineOneK + (highFRLLineOneK * 0.0225))
			--and p.closePrice >= (highFRLLineOneK + (highFRLLineOneK * 0.0125))
			--or p.closePrice <= (medFRLLineOneK + (medFRLLineOneK * 0.0225))
			--and p.closePrice >= (medFRLLineOneK + (medFRLLineOneK * 0.0125))
			--or p.closePrice <= (lowFRLLineOneK + (lowFRLLineOneK * 0.0225))
			--and p.closePrice >= (lowFRLLineOneK + (lowFRLLineOneK * 0.0125))
				THEN 0
				ELSE 4
			END
		END
		ELSE
		CASE WHEN 
		p.closePrice <= (medFRLLineLong * .96)
		and p.momentumA < .947
		--p.closePrice <= (highFRLLineLong + 0.5)
		--and p.closePrice >= (highFRLLineLong - 0.35)
		--or p.closePrice <= (medFRLLineLong + 0.5)
		--and p.closePrice >= (medFRLLineLong - 0.35)
		--or p.closePrice <= (lowFRLLineLong + 0.5)
		--and p.closePrice >= (lowFRLLineLong - 0.35)
			THEN 1
			ELSE
			CASE WHEN 
			p.closePrice >= (highFRLLineLong * .96)
			and p.momentumA > .96
			and (LAG(p.ActualChange, 1) OVER (ORDER BY p.tradeDate) - p.ActualChange) < 0.1
			--p.closePrice <= (highFRLLineOneK + (highFRLLineOneK * 0.0225))
			--and p.closePrice >= (highFRLLineOneK + (highFRLLineOneK * 0.0125))
			--or p.closePrice <= (medFRLLineOneK + (medFRLLineOneK * 0.0225))
			--and p.closePrice >= (medFRLLineOneK + (medFRLLineOneK * 0.0125))
			--or p.closePrice <= (lowFRLLineOneK + (lowFRLLineOneK * 0.0225))
			--and p.closePrice >= (lowFRLLineOneK + (lowFRLLineOneK * 0.0125))
				THEN 0
				ELSE 4
			END
		END
	END
END) AS FRLTradeAction,

-----------------------------------------------------
--1000 day
-----------------------------------------------------
(Case when dbo.fn_Get_TradeAction(p.closePrice,highFRLLineOneK,LAG(p.closePrice,1) OVER (ORDER BY p.tradeDate)
	                                                        ,LAG(p.closePrice,2) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,3) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,4) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,5) OVER (ORDER BY p.tradeDate))=1
	OR dbo.fn_Get_TradeAction(p.closePrice,medFRLLineOneK,LAG(p.closePrice,1) OVER (ORDER BY p.tradeDate)
	                                                        ,LAG(p.closePrice,2) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,3) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,4) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,5) OVER (ORDER BY p.tradeDate))=1
	or dbo.fn_Get_TradeAction(p.closePrice,lowFRLLineOneK,LAG(p.closePrice,1) OVER (ORDER BY p.tradeDate)
	                                                        ,LAG(p.closePrice,2) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,3) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,4) OVER (ORDER BY p.tradeDate)
															,LAG(p.closePrice,5) OVER (ORDER BY p.tradeDate))=1
	THEN 4
	ELSE
	CASE WHEN (p.tradeDate <= DATEADD(month, -6, GETDATE()))
	THEN 
		CASE WHEN p.closePrice <= (highFRLLineOneK * 1.01)
		and p.closePrice >= (highFRLLineOneK * .95)
		THEN 1
			ELSE
			CASE WHEN
			-- closePrice is higher than FRL, but not drastically
			p.closePrice >= (highFRLLineOneK * 1.005)
			and p.closePrice < (highFRLLineOneK * 1.04)
				THEN 0
				ELSE 4
			END
		END
	ELSE
		CASE WHEN p.closePrice <= (highFRLLineOneK * 1.03)
		and p.closePrice >= (highFRLLineOneK * 1.005)
		THEN 1
			ELSE
			CASE WHEN
			-- closePrice is higher than FRL, but not drastically
			p.closePrice < (highFRLLineOneK * 1.075)
			and p.closePrice >= (highFRLLineOneK * 1.025)
				THEN 0
				ELSE 4
			END
		END
	END
END) AS FRL_1K_TradeAction

INTO dbo.GOOG_Act
FROM dbo.GOOG_Pred as p
JOIN dbo.GOOG_TS as ts on ts.tradeDate = p.tradeDate
JOIN dbo.GOOG_Pred2 as p2 on p2.tradeDate = p.tradeDate
ORDER BY p.tradeDate, p2.tradeDate, ts.tradeDate ASC

--------------------------- update fib columns GM------------------
SELECT TOP 1 @fibExtLowNeg = fibExtLowNeg
    , @fibExtHighNeg = [fibExtHighNeg]
    , @fibExtLowPos = [fibExtLowPos] 
    ,@fibExtHighPos = [fibExtHighPos]
FROM dbo.GOOG_Pred2 ORDER BY tradeDate DESC


UPDATE dbo.GOOG_Pred2 SET [fibExtLowNeg] = @fibExtLowNeg , [fibExtHighNeg] = @fibExtHighNeg ,  fibExtHighPos = NULL, fibExtLowPos = NULL
--------------------------- update fib columns GM------------------

END