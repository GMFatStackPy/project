USE [GMFSP_db]
GO
/****** Object:  StoredProcedure [dbo].[sp_Pred2]    Script Date: 12/10/2018 10:34:59 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-----------------------------------------------
-- Author: Chris Del Duco
-- Author: Mohammad Moubadder
--
--			STORED PROCEDURE:
--				sp_Pred2
-----------------------------------------------

ALTER PROCEDURE [dbo].[sp_Pred2]

AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;

-----------------------------------------------
-- FINANCIAL INSTRUMENT:	S&P 500 ETF (SPY)
-- COMPUTATIONS:			FRL Predictions
-- CREATES dbo.SPY_Pred2 from dbo.SPY_Pred and dbo.SPY_TS
-----------------------------------------------
IF EXISTS (SELECT * 
	FROM GMFSP_db.INFORMATION_SCHEMA.TABLES   
	WHERE TABLE_SCHEMA = N'dbo'  AND TABLE_NAME = N'SPY_Pred2')
Drop table dbo.SPY_Pred2
SELECT tick.tradeDate, tick.closePrice, tick.momentumA, tick.momentumB, ts.sPeak, ts.sTrough,
	--If the momentum of the most recent day is less than the 2nd to last day then predict a drop for the next day, reverse is true
	(CASE WHEN LAG(momentumA,0) OVER (ORDER BY tick.tradeDate) < LAG(momentumA,1) OVER (ORDER BY tick.tradeDate)
			AND LAG(momentumA,0) OVER (ORDER BY tick.tradeDate) < 0
			OR LAG(momentumA,0) OVER (ORDER BY tick.tradeDate) < LAG(momentumA,1) OVER (ORDER BY tick.tradeDate)
			AND LAG(momentumA,0) OVER (ORDER BY tick.tradeDate) > 0
	THEN 0
	ELSE
		CASE WHEN LAG(momentumA,0) OVER (ORDER BY tick.tradeDate) > LAG(momentumA,1) OVER (ORDER BY tick.tradeDate)
			--	AND LAG(momentumA,0) OVER (ORDER BY tick.tradeDate) > 0
		THEN 1
		END
	END) AS predNextDay,
	
	-------------------------------------
	--Pos and Neg momentum affects the outcome of frl lines
	-------------------------------------
	(CASE WHEN momentumA < 0.00
		THEN sPeak - ((sPeak - sTrough) * 1.236) 
	END)AS fibExtHighNeg,
	(Case WHEN momentumA < 0.00
		THEN sPeak - ((sPeak - sTrough) * 1.382)
	END) AS fibExtLowNeg,
	--Fib Extension with a positive momentum
	(CASE WHEN momentumA > 0.00
		THEN sPeak + ((sPeak - sTrough) * 1.236)
	END) AS fibExtHighPos,
	(CASE WHEN momentumA > 0.00
		THEN sPeak + ((sPeak - sTrough) * 1.382)
	END) AS fibExtLowPos,
		
	--12 Day Osc
	--(((LAG(closePrice,0) OVER (ORDER BY tick.tradeDate) - troughShort)/ (peakShort - troughShort)) * 100) AS Oscillator
	--100 Day Osc
	(((LAG(closePrice,0) OVER (ORDER BY tick.tradeDate) - lTrough)/ (lPeak - lTrough)) * 100) AS hunOscillator,
	--1k Day Osc
	(((LAG(closePrice,0) OVER (ORDER BY tick.tradeDate) - kTrough)/ (kPeak - kTrough)) * 100) AS oneKOscillator
	
INTO dbo.SPY_Pred2
FROM dbo.SPY_Pred AS tick
JOIN dbo.SPY_TS as ts on ts.tradeDate = tick.tradeDate
ORDER BY tick.tradeDate, ts.tradeDate ASC

-----------------------------------------------
-- FINANCIAL INSTRUMENT:	XPH S&P Pharma ETF (XPH)
-- COMPUTATIONS:			FRL Predictions
-- CREATES dbo.XPH_Pred2 from dbo.XPH_Pred and dbo.XPH_TS
-----------------------------------------------
IF EXISTS (SELECT * 
	FROM GMFSP_db.INFORMATION_SCHEMA.TABLES   
	WHERE TABLE_SCHEMA = N'dbo'  AND TABLE_NAME = N'XPH_Pred2')
Drop table dbo.XPH_Pred2
SELECT tick.tradeDate, tick.closePrice, tick.momentumA, tick.momentumB, ts.sPeak, ts.sTrough,
	--If the momentum of the most recent day is less than the 2nd to last day then predict a drop for the next day, reverse is true
	(CASE WHEN LAG(momentumA,0) OVER (ORDER BY tick.tradeDate) < LAG(momentumA,1) OVER (ORDER BY tick.tradeDate)
			AND LAG(momentumA,0) OVER (ORDER BY tick.tradeDate) < 0
			OR LAG(momentumA,0) OVER (ORDER BY tick.tradeDate) < LAG(momentumA,1) OVER (ORDER BY tick.tradeDate)
			AND LAG(momentumA,0) OVER (ORDER BY tick.tradeDate) > 0
	THEN 0
	ELSE
		CASE WHEN LAG(momentumA,0) OVER (ORDER BY tick.tradeDate) > LAG(momentumA,1) OVER (ORDER BY tick.tradeDate)
			--	AND LAG(momentumA,0) OVER (ORDER BY tick.tradeDate) > 0
		THEN 1
		END
	END) AS predNextDay,
	
	-------------------------------------
	--Pos and Neg momentum affects the outcome of frl lines
	-------------------------------------
	(CASE WHEN momentumA < 0.00
		THEN sPeak - ((sPeak - sTrough) * 1.236) 
	END)AS fibExtHighNeg,
	(Case WHEN momentumA < 0.00
		THEN sPeak - ((sPeak - sTrough) * 1.382)
	END) AS fibExtLowNeg,
	--Fib Extension with a positive momentum
	(CASE WHEN momentumA > 0.00
		THEN sPeak + ((sPeak - sTrough) * 1.236)
	END) AS fibExtHighPos,
	(CASE WHEN momentumA > 0.00
		THEN sPeak + ((sPeak - sTrough) * 1.382)
	END) AS fibExtLowPos,
		
	--12 Day Osc
	--(((LAG(closePrice,0) OVER (ORDER BY tick.tradeDate) - troughShort)/ (peakShort - troughShort)) * 100) AS Oscillator
	--100 Day Osc
	(((LAG(closePrice,0) OVER (ORDER BY tick.tradeDate) - lTrough)/ (lPeak - lTrough)) * 100) AS hunOscillator,
	--1k Day Osc
	(((LAG(closePrice,0) OVER (ORDER BY tick.tradeDate) - kTrough)/ (kPeak - kTrough)) * 100) AS oneKOscillator
		
INTO dbo.XPH_Pred2
FROM dbo.XPH_Pred AS tick
JOIN dbo.XPH_TS as ts on ts.tradeDate = tick.tradeDate
ORDER BY tick.tradeDate, ts.tradeDate ASC

-----------------------------------------------
-- FINANCIAL INSTRUMENT:	PFEIZER (PFE)
-- COMPUTATIONS:			FRL Predictions
-- CREATES dbo.PFE_Pred2 from dbo.PFE_Pred and dbo.PFE_TS
-----------------------------------------------
IF EXISTS (SELECT * 
	FROM GMFSP_db.INFORMATION_SCHEMA.TABLES   
	WHERE TABLE_SCHEMA = N'dbo'  AND TABLE_NAME = N'PFE_Pred2')
Drop table dbo.PFE_Pred2
SELECT tick.tradeDate, tick.closePrice, tick.momentumA, tick.momentumB,	ts.sPeak, ts.sTrough,
	--If the momentum of the most recent day is less than the 2nd to last day then predict a drop for the next day, reverse is true
	(CASE WHEN LAG(momentumA,0) OVER (ORDER BY tick.tradeDate) < LAG(momentumA,1) OVER (ORDER BY tick.tradeDate)
			AND LAG(momentumA,0) OVER (ORDER BY tick.tradeDate) < 0
			OR LAG(momentumA,0) OVER (ORDER BY tick.tradeDate) < LAG(momentumA,1) OVER (ORDER BY tick.tradeDate)
			AND LAG(momentumA,0) OVER (ORDER BY tick.tradeDate) > 0
	THEN 0
	ELSE
		CASE WHEN LAG(momentumA,0) OVER (ORDER BY tick.tradeDate) > LAG(momentumA,1) OVER (ORDER BY tick.tradeDate)
			--	AND LAG(momentumA,0) OVER (ORDER BY tick.tradeDate) > 0
		THEN 1
		END
	END) AS predNextDay,
	
	-------------------------------------
	--Pos and Neg momentum affects the outcome of frl lines
	-------------------------------------
	(CASE WHEN momentumA < 0.00
		THEN sPeak - ((sPeak - sTrough) * 1.236) 
	END)AS fibExtHighNeg,
	(Case WHEN momentumA < 0.00
		THEN sPeak - ((sPeak - sTrough) * 1.382)
	END) AS fibExtLowNeg,
	--Fib Extension with a positive momentum
	(CASE WHEN momentumA > 0.00
		THEN sPeak + ((sPeak - sTrough) * 1.236)
	END) AS fibExtHighPos,
	(CASE WHEN momentumA > 0.00
		THEN sPeak + ((sPeak - sTrough) * 1.382)
	END) AS fibExtLowPos,
		
	--12 Day Osc
	--(((LAG(closePrice,0) OVER (ORDER BY tick.tradeDate) - troughShort)/ (peakShort - troughShort)) * 100) AS Oscillator
	--100 Day Osc
	(((LAG(closePrice,0) OVER (ORDER BY tick.tradeDate) - lTrough)/ (lPeak - lTrough)) * 100) AS hunOscillator,
	--1k Day Osc
	(((LAG(closePrice,0) OVER (ORDER BY tick.tradeDate) - kTrough)/ (kPeak - kTrough)) * 100) AS oneKOscillator
	
INTO dbo.PFE_Pred2
FROM dbo.PFE_Pred AS tick
JOIN dbo.PFE_TS as ts on ts.tradeDate = tick.tradeDate
ORDER BY tick.tradeDate, ts.tradeDate ASC

BEGIN
	exec dbo.sp_CMA_FRL_ACT;
END

END
