import sqlalchemy as sal
from statistics import stdev
import math
import numpy as np
import datetime as datetime


class Forecast:
    def __init__(self, f2):
        self.engine = f2.sql_engine()
        self.sources = f2.get_datasources(self.engine, 'dbo.FinDataSources')
        self.fc_data = f2.get_data(self.engine, self.sources)

    def compute_forecast(self):
        """
        PLACEHOLDER: Computations will be computed by various means and be updated as necessary

        :return: none
        """
        f_data = self.fc_data
        n = 0

        # Cycle through each ticker symbol
        while n < len(self.sources.index):
            # Create variable names for the <ticker>_Forecast table.
            ticker = self.sources.iat[n, self.sources.columns.get_loc('dataSource')]
            forecast_table = ticker + '_Forecast'

            # TEST: Verify order of data (tradeDate ASC)
            # self.fc_data[n].to_csv('fc_Data.csv')

            f_data[n]['momentumA'] = self.fc_data[n]['closePrice'].diff(10)
            f_data[n]['sTrough'] = self.fc_data[n]['closePrice'].tail(14).min()
            f_data[n]['sPeak'] = self.fc_data[n]['closePrice'].tail(14).max()
            f_data[n]['lagMomentum'] = f_data[n]['momentumA'].shift(5)
            f_data[n]['kTrough'] = self.fc_data[n]['closePrice'].tail(1000).min()
            f_data[n]['kPeak'] = self.fc_data[n]['closePrice'].tail(1000).max()

            f_data[n]['kHighFibLine'] = f_data[n]['kPeak'] - ((f_data[n]['kPeak'] - f_data[n]['kTrough']) * 0.236)
            f_data[n]['kMedFibLine'] = f_data[n]['kPeak'] - ((f_data[n]['kPeak'] - f_data[n]['kTrough']) * 0.382)
            f_data[n]['kLowFibLine'] = f_data[n]['kPeak'] - ((f_data[n]['kPeak'] - f_data[n]['kTrough']) * 0.618)
            f_data[n]['kMaxFibLine'] = f_data[n]['kPeak'] - ((f_data[n]['kPeak'] - f_data[n]['kTrough']) * 1.0)

            kHighFibLine = f_data[n]['kHighFibLine']

            avg_days = np.average(self.fc_data[n]['closePrice'].tail(14))
            std_days = stdev(self.fc_data[n]['closePrice'].tail(14), avg_days)

            if f_data[n]['momentumA'].tail(1).iloc[0] < 0.00:
                f_data[n]['fibExtHighNeg'] = f_data[n]['sPeak'] - (
                        (f_data[n]['sPeak'] - f_data[n]['sTrough']) * 1.236)
                f_data[n]['fibExtLowNeg'] = f_data[n]['sPeak'] - (
                        (f_data[n]['sPeak'] - f_data[n]['sTrough']) * 1.382)
                f_data[n]['fibExtHighPos'] = None
                f_data[n]['fibExtLowPos'] = None
            elif f_data[n]['momentumA'].tail(1).iloc[0] > 0.00:
                f_data[n]['fibExtHighPos'] = f_data[n]['sPeak'] + (
                        (f_data[n]['sPeak'] - f_data[n]['sTrough']) * 1.236)
                f_data[n]['fibExtLowPos'] = f_data[n]['sPeak'] + (
                        (f_data[n]['sPeak'] - f_data[n]['sTrough']) * 1.382)
                f_data[n]['fibExtHighNeg'] = None
                f_data[n]['fibExtLowNeg'] = None

            # Drop unnecessary column data
            f_data[n].drop(['openPrice', 'low', 'high', 'volume', 'wCMA',
                            'lTrough', 'lPeak', 'kTrough', 'kPeak', 'BUYweekApproach', 'week_Long',
                            'SELLweekApproach', 'week_Short', 'highFRLLineLong',
                            'medFRLLineLong', 'lowFRLLineLong', 'highFRLLineOneK', 'medFRLLineOneK', 'lowFRLLineOneK',
                            'momentumB', 'rateOfChange', 'ActualChange', 'hunOscillator', 'oneKOscillator'],
                           axis=1, inplace=True)

            # Send Trading Strategy data to SQL dbo.<Ticker>_Forecast
            f_data[n].to_sql(forecast_table, self.engine, if_exists='replace', index=False,
                             dtype={'tradeDate': sal.Date, 'closePrice': sal.FLOAT, 'recentMomentumA': sal.FLOAT,
                                    'FRLTradeAction': sal.FLOAT, 'FRL_1K_TradeAction': sal.FLOAT,
                                    'momentumA': sal.FLOAT, 'predNextDay': sal.FLOAT, 'fibExtHighNeg': sal.FLOAT,
                                    'fibExtLowNeg': sal.FLOAT, 'fibExtHighPos': sal.FLOAT, 'fibExtLowPos': sal.FLOAT,
                                    'sPeak': sal.FLOAT, 'sTrough': sal.FLOAT})

            today = datetime.datetime.now()
            # number of weekdays
            weekdays = 10
            # 3 weeks of weekdays
            days = 15
            predict = []

            # for i in range # of weekdays
            for i in range(weekdays):
                # if today is Friday...
                if today.weekday() == 4:
                    # ...increase date to upcoming Monday
                    today += datetime.timedelta(days=3)
                else:
                    # ...else increase date to tomorrow
                    today += datetime.timedelta(days=1)

                # if the momentum is negative
                if f_data[n]['momentumA'].tail(1).iloc[0] < 0.00:
                    # ...Set fibonacci extensions accordingly
                    f_data[n]['fibExtHighNeg'] = f_data[n]['sPeak'] - (
                            (f_data[n]['sPeak'] - f_data[n]['sTrough']) * 1.236)
                    f_data[n]['fibExtLowNeg'] = f_data[n]['sPeak'] - (
                            (f_data[n]['sPeak'] - f_data[n]['sTrough']) * 1.382)
                    f_data[n]['kTrough'] = self.fc_data[n]['closePrice'].tail(1000).min()
                    f_data[n]['kPeak'] = self.fc_data[n]['closePrice'].tail(1000).max()
                    f_data[n]['kHighFibLine'] = f_data[n]['kPeak'] - \
                                                ((f_data[n]['kPeak'] - f_data[n]['kTrough']) * 0.236)
                    kHighFibLine = f_data[n]['kHighFibLine'].tail(1).iloc[0]
                    # ...Compute average over last 3 weeks of weekdays
                    avg_days = np.average(self.fc_data[n]['closePrice'].tail(days))
                    # ...Compute standard Deviation over the last 3 weeks and the average.
                    std_days = stdev(self.fc_data[n]['closePrice'].tail(days), avg_days)
                    # ...Compute Standard Error and apply to variable decrease
                    decrease = avg_days - (1.960 * std_days) / (math.sqrt(days))
                    f_data[n]['fibExtHighPos'] = None
                    f_data[n]['fibExtLowPos'] = None
                    l_cma = f_data[n]['lCMA'].tail(1)
                    frl = f_data[n]['sTrough'].tail(1)
                    l_cma = l_cma.values[0]
                    frl = frl.values[0]
                    # For each upcoming day in the week...
                    for x in range(weekdays):
                        # ...compare to current location of cma and frl values
                        # if CMA and FRL are lower than forecast
                        # ...forecast lower with a medium magnitude
                        if decrease > l_cma or decrease >= (kHighFibLine + (kHighFibLine * 0.01)):
                            decrease -= .5 * std_days
                            predict.append(decrease)
                            # print(predict[x])
                        # if CMA and FRL are higher than forecast
                        # ...forecast a rise with a aggressive magnitude
                        elif decrease <= l_cma and decrease <= (kHighFibLine - (kHighFibLine * 0.01)):
                            decrease += 1.5 * std_days
                            predict.append(decrease)
                    x = x + 1

                # if the momentum is positive
                elif f_data[n]['momentumA'].tail(1).iloc[0] > 0.00:
                    # ...Set fibonacci extensions accordingly
                    f_data[n]['fibExtHighPos'] = f_data[n]['sPeak'] + (
                            (f_data[n]['sPeak'] - f_data[n]['sTrough']) * 1.236)
                    f_data[n]['fibExtLowPos'] = f_data[n]['sPeak'] + (
                            (f_data[n]['sPeak'] - f_data[n]['sTrough']) * 1.382)
                    f_data[n]['kTrough'] = self.fc_data[n]['closePrice'].tail(1000).min()
                    f_data[n]['kPeak'] = self.fc_data[n]['closePrice'].tail(1000).max()
                    f_data[n]['kHighFibLine'] = f_data[n]['kPeak'] - \
                                                ((f_data[n]['kPeak'] - f_data[n]['kTrough']) * 0.236)
                    kHighFibLine = f_data[n]['kHighFibLine'].tail(1).iloc[0]
                    # ...Compute average over last 3 weeks of weekdays
                    avg_days = np.average(self.fc_data[n]['closePrice'].tail(days))
                    # ...Compute standard Deviation over the last 3 weeks and the average.
                    std_days = stdev(self.fc_data[n]['closePrice'].tail(days), avg_days)
                    # ...Compute Standard Error and apply to variable increase
                    increase = avg_days + (1.960 * std_days) / (math.sqrt(days))
                    f_data[n]['fibExtHighNeg'] = None
                    f_data[n]['fibExtLowNeg'] = None
                    l_cma = f_data[n]['lCMA'].tail(1)
                    frl = f_data[n]['sTrough'].tail(1)
                    l_cma = l_cma.values[0]
                    frl = frl.values[0]
                    for x in range(weekdays):
                        # ...compare to current location of cma and frl values
                        # if CMA and FRL are lower than forecast
                        # ...forecast lower with a normal magnitude
                        if increase > l_cma and increase >= (kHighFibLine - (kHighFibLine * 0.01)):
                            increase -= std_days
                            predict.append(increase)
                            # print(predict[x])
                        # if CMA and FRL are lower than forecast
                        # ...forecast lower with an aggressive magnitude
                        elif increase <= l_cma or increase <= (kHighFibLine - (kHighFibLine * 0.01)):
                            increase += 1.5 * std_days
                            predict.append(increase)
                            # print(predict[x])

                fibExtHighNeg = f_data[n]['fibExtHighNeg'].tail(1).iloc[0]
                fibExtLowNeg = f_data[n]['fibExtLowNeg'].tail(1).iloc[0]
                fibExtHighPos = f_data[n]['fibExtHighPos'].tail(1).iloc[0]
                fibExtLowPos = f_data[n]['fibExtLowPos'].tail(1).iloc[0]

                kHighFibLine = f_data[n]['kHighFibLine']
                kMedFibLine = f_data[n]['kMedFibLine']
                kLowFibLine = f_data[n]['kLowFibLine']
                kMaxFibLine = f_data[n]['kMaxFibLine']

                sTrough = f_data[n]['sTrough'] = self.fc_data[n]['closePrice'].tail(14).min()
                sPeak = f_data[n]['sPeak'] = self.fc_data[n]['closePrice'].tail(14).max()

                # Date Variables
                closeDate = today.strftime("%Y-%m-%d")
                # Send the addition of new variables to SQL
                query = "INSERT INTO %s([tradeDate], [closePrice], [sTrough], [sPeak], [fibExtHighNeg], [fibExtLowNeg], [fibExtHighPos], [fibExtLowPos]) " \
                        "values(?, ?, ?, ?, ?, ?, ?, ?)" % forecast_table
                self.engine.execute(query, closeDate, predict[i], sTrough, sPeak, fibExtHighNeg, fibExtLowNeg, fibExtHighPos, fibExtLowPos)

            # Advance source ticker counter
            n = n + 1

    def check_db(self):
        """
        Method that verifies the database is ordered properly and rectifies it if not.
        Runs after strategy and before forecast to ensure integrity.
        If the database has table <ticker>_TS it will run as normal.
        Else it will return false, signalling the application to reprocess the data.

        :return rsg: Boolean, ready set go ( true ? false )
        """
        n = 0
        while n < len(self.sources.index):
            # Create variable names for each ticker table.
            # Used to query for all financial data related to ticker symbol stored in GMFSP_db.
            ticker = self.sources.iat[n, self.sources.columns.get_loc('dataSource')]
            ticker_ts = ticker + '_TS'

            # Check if Trading Strategies table exists
            if self.engine.dialect.has_table(self.engine, ticker_ts):
                return False
            else:
                n = n + 1

        return True
