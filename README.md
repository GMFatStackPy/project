GM Tableau
FATSTAcK Py
README

Chris Del Duco | Mohamad Moubadder | CSC4996 | Dec. 6 2018



Introduction
	The application’s one end function, is to find trading opportunities given historical data with minimal user interaction.  It does this by analyzing the historical data, applying Cross Moving Average and Fibonacci Retracement Line principles, and automates the decision making process of buying and selling on these principles.
Getting Started
These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

Prerequisites
    Python 3.6 (or newer)
    SQL Server 2017
    Tableau Desktop 2018
    
    See APPENDIX A for additional software requirements.

Installing
1.	Create FATSTAcKPy folder in convenient location on machine
2.	Clone master directory of GitLab Repository
        git clone https://gitlab.com/GMFatStackPy/project.git	
3.	Launch Microsoft SQL Server Management Studio
    a.	Ensure SQL Server Service is running.
    b.	Move Backup File from cloned directory to SQL Server Back up Directory.
    c.	Restore GMFSP_db.
4.	Install Anaconda 3.6 ( or newer )
    a.	Ensure Anaconda is added to PATH is checked.
    b.	Install Pycharm (recommended).
        i.	Ensure SQL Server Drivers are updated.
5.	Ensure all dependencies are installed
    a.	See APPENDIX A
6.	Run Python Code.
7.	Open Tableau Dashboard from cloned directory
    a.	Check Tableau Dashboard.

Running the tests
See accompanying Testing Document.

Contributing
Authors
•	Chris Del Duco – Team Lead / Development
•	Mohamad Moubadder – Development 

License
This project is licensed under the MIT License.

APPENDIX A
Dependencies:
	pyODBC
	SQLAlchemy
	Pandas
	Pandas-datareader
	DateTime
	Statistics
	Math
	NumPy
	pyTest
	MockTest
	* Running Anaconda Environment satisfies some of these dependencies and is recommended
Python:
●	Anaconda Environments & Managing Them
Trading Strategies:
●	Cross Moving Averages Explained
●	Fibonacci Retracement - Python
ML
○	Machine Learning Techniques & Use Cases
○	ML & Classic Trading Strategies
○	Neural Nets & Trading
●	https://www.youtube.com/watch?v=vJ2lYsDSgg0 - Multiple Lines in Tableau
Tableau:
●	Tableau Training Videos
●	Tableau Community - Python Step By Step
●	Tableau Community - Python Getting Started
●	Tableau Community - Developer Portal
●	Tableau Community - Python Integration
●	Tableau - Python SQL Server
●	Tableau Expert Path Guide
●	Tableau Python Anaconda TabPy Example
TabPy
○	TabPy Python Client
○	TabPy ReadMe File
Data Sources:
●	IEX Trading (IEXTrading.com)
●	Quandl (Quandl.com)
●	Yahoo Finance (.csv)
●	Great Idea Generator and informative Data Source
●	Knoema - Statistical Data about USA (Not entirely applicable, Very Interesting)
Git Usage:
ELI5:
○	ELI5 - Commands, Process, etc.
○	Nvie.com - Successful Branching Model

 



Technologies & Software:

SQL Server 2017 Download

http://apps.eng.wayne.edu/MPStudents/Dreamspark.aspx

SQL Server 2017 Enterprise Install

https://docs.microsoft.com/en-us/sql/advanced-analytics/install/sql-machine-learning-services-windows-install?view=sql-server-2017

Anaconda Environment

https://www.anaconda.com/download/#windows

PyCharm free student

https://www.jetbrains.com/student/

Tableau free student

https://www.tableau.com/academic/students

GitLab

https://about.gitlab.com


